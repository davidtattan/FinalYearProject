#!/usr/bin/python

import plotly.offline as py
import plotly.plotly as pyo
import plotly.graph_objs as go
import os
import math
import sys
import numpy as np
from numpy import linalg as LA
from statistics.tauint.puwr import tauint, correlated_data
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from scipy.integrate import quad
from scipy.integrate import romberg
from statistics.tauint.puwr import tauint, correlated_data

rc('text', usetex=True)


def subtract_vacuum(directory, V_E): #Artifical vacuum subtraction. V_E - vacuum energy
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(directory)
                    delta = f[:-4].split("_")[1]
                    new_dir = os.path.join(head,tail+'_edited')
                    if not os.path.exists(new_dir):
                        os.makedirs(new_dir)
                    f2 = open(os.path.join(new_dir,'delta_'+str(delta)+'.txt'),'w')
                    content = myfile.readlines()
                    content = [float(x) - V_E for x in content]
                    for x in content:
                        f2.write(str(x)+'\n')
  

def plot_MC_steps(f): #Plot data as a function of MC time.
    t = []
    MC_steps = []
    with open(f,'r') as myfile:
        content = myfile.readlines()
        for i in range(0,len(content)):
            t.append(i)
            MC_steps.append(content[i])

    plt.plot(t, MC_steps)
    plt.legend(fontsize=12,loc=2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
#    plt.axes().set_aspect(0.5)
    plt.xlabel('$t_{MC}$')
    plt.ylabel('$\\varphi$')
#    plt.xlim((0,49))
#    plt.ylim((0,1.2))
    pp = PdfPages('MC_plot.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    return

def plot_correlation(rootdir): #Just plot the correlation data contained in rootdir.
    deltas = []
    corrs = []
    err = []

    for subdir1, dirs1, files1 in os.walk(rootdir):
        for subdir, dirs, files in os.walk(subdir1):
            tau_int_max = 0
            d_tau_max = 0
            for f in files:
                if f.split('.')[1] != 'txt':
                    pass
                else:
                    delta = f[:-4].split("_")[1]
                    deltas.append(delta)
                    with open(os.path.join(subdir,f),'r') as myfile:
                        var = 0
                        content = myfile.readlines()
                        content = [float(x.strip()) for x in content]
                        corr = sum(content)/len(content)
                        for i in range(0,len(content)):
                            x = (sum(content) - content[i])/(len(content)-1)
                            var += (corr - x)**2
                        error = math.sqrt((len(content)-1)*var/len(content))
                        corrs.append(corr)
                        err.append(error)
    
#    print "Average = ", sum(corrs)/len(deltas)
    plt.errorbar(deltas, corrs, err, fmt='k.')
    plt.legend(fontsize=12,loc=2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
#    plt.axes().set_aspect(0.0006)
    plt.xlabel('$t$')
    plt.ylabel('$C_{11}(t)$')
#    plt.xlim((0,49))
#    plt.ylim((0,1.2))
    pp = PdfPages('correlation_plot.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    return

def sort_raw_data(raw_dir, slice_length): #Slice and save data into chunks of length slice_length
    for subdir, dirs, files in os.walk(raw_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    content = myfile.readlines()
                    for i in range(0,len(content)/slice_length):
                        head, tail = os.path.split(raw_dir)
                        delta = f[:-4].split("_")[1]
                        new_dir = os.path.join(head,'processed_data')
                        new_dir = os.path.join(new_dir,str(i))
                        if not os.path.exists(new_dir):
                            os.makedirs(new_dir)
                        f2 = open(os.path.join(new_dir,'delta_'+str(delta)+'.txt'),'w')
                        sliced_content = content[i*slice_length:(i+1)*slice_length]
                        for j in range(0,len(sliced_content)):
                            x = sliced_content[j]
                            f2.write(x)
    return

def extract_tauint(raw_dir_master, slice_length, no_measure): #A really ugly function for plotting tau_int. Ugly because I saved data in a stupid way, turned out to be not very useful.
    tau_array = {}
    dtau_array = {}
    for i in range(0,no_measure):
        raw_dir = os.path.join(raw_dir_master, str(i))
        sort_raw_data(raw_dir, slice_length)
        new_dir = os.path.join(raw_dir_master,'processed_data')
        for subdir, dirs, files in os.walk(new_dir):
            for f in files:
                if f.split('.')[1] != 'txt':
                    pass
                else:
                    delta = str(f[:-4].split("_")[1])
                    head, tail = os.path.split(subdir)
                    with open(os.path.join(subdir,f),'r') as myfile:
                        a = []
                        a.append([])
                        a[0].append([])
                        content = myfile.readlines()
                        for i in range(len(content)/2,len(content)):
                            x = content[i]
                            x = x.strip()
                            content[i] = float(x)
                            a[0][0].append(float(x))
                        mean, error, tau_int, d_tau = tauint(a,0)
                        i = 0
                        try:
                            tau_array[tail][delta].append(tau_int)
                            dtau_array[tail][delta].append(d_tau)
                        except:
                            try:
                                tau_array[tail][delta] = []
                                dtau_array[tail][delta] = []
                                tau_array[tail][delta].append(tau_int)
                                dtau_array[tail][delta].append(d_tau)
                            except:
                                tau_array[tail] = {}
                                dtau_array[tail] = {}
                                tau_array[tail][delta] = []
                                dtau_array[tail][delta] = []
                                tau_array[tail][delta].append(tau_int)
                                dtau_array[tail][delta].append(d_tau)
    
    stds = []
    avgs = []
    avg_ers = []
    for stdint in tau_array.keys():
        std = 0.25*int(stdint) + 0.01
        stds.append(std)
        max_avg = 0
        max_er = 0
        for delta in tau_array[stdint]:
            ers = [x**2 for x in dtau_array[stdint][delta]] 
            avg = sum(tau_array[stdint][delta])/len(tau_array[stdint][delta])
            avg_er = math.sqrt(sum(ers))/len(ers)
            if avg > max_avg:
                max_avg = avg
                max_er = avg_er
        avgs.append(max_avg)
        avg_ers.append(max_er)
    
    trace = go.Scatter(
        x = stds,
        y = avgs,
        mode = 'markers',
        error_y=dict(
                type='data',
                array=avg_ers,
                visible=True
                )
        )
    
    layout=go.Layout(title="")
    data = [trace]
    figure = go.Figure(data=data,layout=layout)
    py.plot(figure)
    return
    
def rebin(directory,bin_size): #Standard rebinning of data into bins of size bin_size.
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(directory)
                    delta = f[:-4].split("_")[1]
                    new_dir = os.path.join(head,tail+'_bin')
                    if not os.path.exists(new_dir):
                        os.makedirs(new_dir)
                    f2 = open(os.path.join(new_dir,'delta_'+str(delta)+'.txt'),'w')
                    content = myfile.readlines()
                    no_bins = len(content)/bin_size
                    for i in range(0,no_bins):
                        content_short = content[i*len(content)/no_bins:(i+1)*len(content)/no_bins]
                        for j in range(0,len(content_short)):
                            x = content_short[j]
                            x = x.strip()
                            content_short[j] = float(x)
                        avg = sum(content_short)/len(content_short)
                        f2.write(str(avg)+'\n')

def crop(directory,length): #Crop each file in directory to a given length. Useful to have a round number before binning.
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(directory)
                    delta = f[:-4].split("_")[1]
                    new_dir = os.path.join(head,tail+'_crop')
                    if not os.path.exists(new_dir):
                        os.makedirs(new_dir)
                    f2 = open(os.path.join(new_dir,'delta_'+str(delta)+'.txt'),'w')
                    content = myfile.readlines()
                    content = content[len(content)-length:len(content)]
                    #content = content[0:len(content)-length]
                    i = 0
                    for x in content:
                        f2.write(x)

def jackknife(): #Think this was just a test, never really used.
    for subdir, dirs, files in os.walk('/home/david/FinalYearProject/data/correlations/3Dec_5_edited'):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = f[:-4].split("_")[1]
                    f2 = open('/home/david/FinalYearProject/data/correlations/3Dec_5_jk/delta_'+str(delta)+'.txt','w')
                    content = myfile.readlines()
                    content = [float(x) for x in content]
                    for i in range(0,len(content)):
                        s = sum(content) - content[i]
                        avg = s/(len(content)-1)
                        f2.write(str(avg)+'\n')

def jackknife_log(directory): #Plot the log of the correlation data, using jackknife resampling.
    max_delta = 0
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    deltas = []
    logs = []
    logs_errors = []
    error_naive = []
    for t in range(0,max_delta):
        with open(os.path.join(directory,'delta_'+str(t)+'.txt')) as myfile:
            deltas.append(t)
            content1 = myfile.readlines()
            try:
                content1 = [float(x) for x in content1]
                r = math.log(sum(content1)/len(content1))
                r_corr = 0
                jk_sum = 0
                varjk = 0
                for i in range(0,len(content1)):
                    s1 = (sum(content1) - content1[i])/(len(content1)-1)
                    varjk += (r - math.log(s1))**2
                    jk_sum += len(content1)*r - (len(content1)-1)*math.log(s1)
                mean = jk_sum/len(content1)
                error = math.sqrt((len(content1)-1)*varjk/len(content1))
                
            except:
                mean = error = 0
            logs.append(mean)
            logs_errors.append(error)
    
    plt.errorbar(deltas, logs, logs_errors, fmt='k.')
    
    plt.legend(fontsize=12,loc=2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
    plt.axes().set_aspect(3.8)
    plt.xlabel('$t$')
    plt.ylabel('$\\log\\ C_{11}(t)$')
#    plt.xlim((0,10))
#    plt.ylim((0,1.2))
    pp = PdfPages('log_11correlation_plot.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()

    return

def jackknife_log_ratio(directory): #Plot the log of the slope (=ratio) of correlation data, again using jackknife.
    max_delta = 0
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    deltas = []
    logs = []
    logs_errors = []
    error_naive = []
    for t in range(0,int(math.ceil(max_delta/2))):
        with open(os.path.join(directory,'delta_'+str(t)+'.txt')) as myfile:
            with open(os.path.join(directory,'delta_'+str(t+1)+'.txt')) as myfile2:
                deltas.append(t)
                content1 = myfile.readlines()
                content2 = myfile2.readlines()
                try:
                    content1 = [float(x) for x in content1]
                    content2 = [float(x) for x in content2]
                    r = math.log(sum(content1)/sum(content2))
                    r_corr = 0
                    jk_sum = 0
                    varjk = 0
                    for i in range(0,len(content1)):
                        s1 = sum(content1) - content1[i]
                        s2 = sum(content2) - content2[i]
                        varjk += (r - math.log(s1/s2))**2
                        jk_sum += len(content1)*r - (len(content1)-1)*math.log((s1/s2))
                    mean = jk_sum/len(content1)
                    error = math.sqrt((len(content1)-1)*varjk/len(content1))
                    
                except:
                    mean = error = 0
                logs.append(mean)
                logs_errors.append(error)
    
    #print logs[0], logs_errors[0]
    plt.errorbar(deltas, logs, logs_errors, fmt='k.')
    
    plt.legend(fontsize=12,loc=2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
#    plt.axes().set_aspect(13)
    plt.xlabel('$t$')
    plt.ylabel('$\\textrm{log}\\left( \\frac{C_{33}(t)}{C_{33}(t+1)} \\right)$')
#    plt.xlim((0,10))
#    plt.ylim((0,1.2))
    pp = PdfPages('log_33correlation_plot.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()

    return logs[0], logs_errors[0]

def dispersion_plot(): #Plot the dispersion relation. A lot of hardcoding in here, but only needed it once really.
    E_array = []
    dE_array = []
    pi = math.pi
    def p(x,y,z):
        return math.sqrt((2*pi*x/8)**2 + (2*pi*y/8)**2 + (2*pi*z/8)**2)
    p_array = [p(0,0,0),p(1,0,0),p(0,1,0),p(0,0,1),p(2,0,0),p(3,0,0),p(1,1,0),p(1,0,1),p(0,1,1),p(1,1,1),p(2,2,0),p(2,0,2),p(2,2,2),p(3,3,3)]
    for i in range(1,15):
        E, dE = jackknife_log_ratio('/home/david/FinalYearProject/data/11_correlations/12Feb_'+str(i)+'_crop_bin')
        E_array.append(E)
        dE_array.append(dE)
        print E, dE
    
    p = 0
    pan_array = []
    Ean_array = []
    Ean_error = []
    while p<4.1:
        Ean_array.append(math.sqrt(0.27515009652**2 + p**2))
        pan_array.append(p)
        Ean_error.append(0.27515009652*0.000918572571177/math.sqrt(0.276188956319**2 + p**2))
        p += 0.1
    
    plt.plot(pan_array, Ean_array, 'k--', label='$\\textrm{Continuum Dispersion}$')
    plt.errorbar(p_array, E_array, yerr=dE_array, fmt='k.', label='$8^{3}\\textrm{x}50\\textrm{ Lattice Dispersion}$')
    
    plt.legend(fontsize=12, loc=2)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
    plt.axes().set_aspect(0.5)
    plt.xlabel('$\\vert\\vec{p}\\vert$')
    plt.ylabel('$E_{1}(0;\\vec{p})$')
    pp = PdfPages('dispersion.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    
    trace = go.Scatter(
    x = p_array,
    y = E_array,
    mode = 'markers',
    error_y=dict(
            type='data',
            array=dE_array,
            visible=True
        )
    )
    
    data = [trace]
    py.plot(data)
    
    return

def plot_tauint(directory):
    deltas = []
    tau_ints = []
    tau_errors = []
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    a = []
                    a.append([])
                    a[0].append([])
                    delta = f[:-4].split("_")[1]
                    deltas.append(float(delta))
                    content = myfile.readlines()
                    for i in range(0,len(content)):
                        x = content[i]
                        x = x.strip()
                        content[i] = float(x)
                        a[0][0].append(float(x))
                    mean, error, tau_int, d_tau = tauint(a,0)
                    tau_ints.append(tau_int)
                    tau_errors.append(d_tau)

    trace = go.Scatter(
    x = deltas,
    y = tau_ints,
    mode = 'markers',
    error_y=dict(
            type='data',
            array=tau_errors,
            visible=True
            )
    )

    layout=go.Layout(title="")
        
    data = [trace]
    figure = go.Figure(data=data,layout=layout)
    py.plot(figure)
    return

#GEVP FUNCTIONS
def c_matrix_odd(c11_dir,c13_dir,c33_dir, t, jk_omit=None): #Forms C(t) for odd parity states. jk_omit is for performing Jackknife analysis
    for subdir, dirs, files in os.walk(c11_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c11_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        for i in range(0,len(content)):
                            if i == jk_omit:
                                pass
                            else:
                                x = content[i]
                                x = x.strip()
                                data = data + float(x)
                                no_data += 1
                        c_11 = data/no_data
                        
    for subdir, dirs, files in os.walk(c13_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c13_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        for i in range(0,len(content)):
                            if i == jk_omit:
                                pass
                            else:
                                x = content[i]
                                x = x.strip()
                                data = data + float(x)
                                no_data += 1
                        c_13 = data/no_data
                        
    for subdir, dirs, files in os.walk(c33_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c33_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        for i in range(0,len(content)):
                            if i == jk_omit:
                                pass
                            else:
                                x = content[i]
                                x = x.strip()
                                data = data + float(x)
                                no_data += 1
                        c_33 = data/no_data

    c = np.matrix([[c_11, c_13], [c_13, c_33]])
    return c

def sqrt_c_odd(c11_dir,c13_dir,c33_dir, t0, jk_omit=None): #Forms C^{1/2}(t0) for odd parity states.
    c_t0 = c_matrix_odd(c11_dir, c13_dir, c33_dir, t0, jk_omit)
    l, U = LA.eig(c_t0)
    sqrt_diag = np.matrix([[math.sqrt(l[0]), 0], [0, math.sqrt(l[1])]])
    sq_c = U*sqrt_diag*np.transpose(U)
    return sq_c

def e_vals_odd(c11_dir, c13_dir, c33_dir, t0, t, jk_omit=None): #Solves the GEVP for odd parity states
    c = c_matrix_odd(c11_dir,c13_dir,c33_dir, t, jk_omit)
    sq_c = sqrt_c_odd(c11_dir,c13_dir,c33_dir, t0, jk_omit)
    sq_c_inv = LA.inv(sq_c)
    W = sq_c_inv*c*sq_c_inv
    l,v = LA.eig(W)
    l = sorted(l)
    
    #if l[0]>l[1]:
        #return -math.log(l[0])/(t-t0), -math.log(l[1])/(t-t0), math.log(l[1])/math.log(l[0])
    #else:
        #return -math.log(l[1])/(t-t0), -math.log(l[0])/(t-t0), math.log(l[0])/math.log(l[1])
    
    c_1 = c_matrix_odd(c11_dir,c13_dir,c33_dir, t+1, jk_omit)
    sq_c_1 = sqrt_c_odd(c11_dir,c13_dir,c33_dir, t0, jk_omit)
    sq_c_inv_1 = LA.inv(sq_c_1)
    W_1 = sq_c_inv_1*c_1*sq_c_inv_1
    l_1,v_1 = LA.eig(W_1)
    l_1= sorted(l_1)
    
    logs = [math.log(l_1[0]),math.log(l_1[1]),math.log(l[0]),math.log(l[1])]
    logs = sorted(logs)
    
    E = [logs[3]-logs[2], logs[1]-logs[0]]
    E = sorted(E)
    return E[0], E[1], E[1]/E[0]

def e_vals_error_odd(c11_dir, c13_dir, c33_dir, t0, t, E0, E1): #Jackknife analysis for finding errors on extracted energy levels
    #First find how many measurements we have
    for subdir, dirs, files in os.walk(c11_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c11_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        length = len(content)
    
    var0 = []
    var1 = []
    var_ratio = []
    for j in range(0, length):
        e0, e1, ratio = e_vals_odd(c11_dir, c13_dir, c33_dir, t0, t, j)
        var0.append((E0-e0)**2)
        var1.append((E1-e1)**2)
        var_ratio.append((E1/E0 - ratio)**2)
    dE0 = math.sqrt((length-1)*sum(var0)/length)
    dE1 = math.sqrt((length-1)*sum(var1)/length)
    dR = math.sqrt((length-1)*sum(var_ratio)/length)
    return dE0, dE1, dR

def e_vals_plot_odd(c11_dir, c13_dir, c33_dir, t0): #Puts all of the above together and plots
    max_delta = 0
    for subdir, dirs, files in os.walk(c11_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    t_list = []
    E0_list = []
    E1_list = []
    dE0_list = []
    dE1_list = []
    for t in range(t0+1,int(math.ceil(max_delta/2))):
        try:
            E0, E1, R = e_vals_odd(c11_dir, c13_dir, c33_dir, t0, t)
            dE0, dE1, dR = e_vals_error_odd(c11_dir, c13_dir, c33_dir, t0, t, E0, E1)
            #print R, "+-", dR
            if dE0<dE1:
                t_list.append(t)
                E0_list.append(E0)
                E1_list.append(E1)
                dE0_list.append(dE0)
                dE1_list.append(dE1)
            else:
                t_list.append(t)
                E0_list.append(E1)
                E1_list.append(E0)
                dE0_list.append(dE1)
                dE1_list.append(dE0)
            print R, dR
        except:
            pass
    
    plt.errorbar(t_list, E0_list, yerr=dE0_list, fmt='k,--',color='#067c0c', label='$E_{1}$')
    plt.errorbar(t_list, E1_list, yerr=dE1_list, fmt='k,:', color='#3e1191', label='$E_{3}$')
    
    plt.legend(fontsize=12,loc=1)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
#    plt.axes().set_aspect(65)
    plt.xlabel('$t$')
    plt.ylabel('$E_{k}$')
#    plt.xlim((0,10))
#    plt.ylim((0,1.2))
    pp = PdfPages('odd_spectrum.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    return

#Should combine these sets of functions, "_odd" and "_even", into one set that takes "e" or "o" as argument, or something similar.
#The functions below do the same as above, just for even parity states.

def c_matrix_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t, jk_omit=None):
    max_delta = 0
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    N_t = max_delta + 1
    
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile1:
                    with open(os.path.join(phi_sq_dir,f), 'r') as myfile2:
                        head, tail = os.path.split(c22_dir)
                        delta = f[:-4].split("_")[1]
                        if int(delta) != t:
                            pass
                        else:
                            data1 = 0
                            data2 = 0
                            no_data = 0
                            content_22 = myfile1.readlines()
                            content_phisq = myfile2.readlines()
                            for i in range(0,len(content_22)):
                                if i == jk_omit:
                                    pass
                                else:
                                    x = content_22[i]
                                    x = x.strip()
                                    data1 = data1 + float(x)
                                    y = content_phisq[i]
                                    y = y.strip()
                                    data2 = data2 + float(y)
                                    no_data += 1
                            c_22 = data1/no_data - N_t*data2*data2/(no_data*no_data)
     
     
    for subdir, dirs, files in os.walk(c44_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile1:
                    with open(os.path.join(phi_fourth_dir,f), 'r') as myfile2:
                        head, tail = os.path.split(c44_dir)
                        delta = f[:-4].split("_")[1]
                        if int(delta) != t:
                            pass
                        else:
                            data1 = 0
                            data2 = 0
                            no_data = 0
                            content_44 = myfile1.readlines()
                            content_phi4 = myfile2.readlines()
                            for i in range(0,len(content_44)):
                                if i == jk_omit:
                                    pass
                                else:
                                    x = content_44[i]
                                    x = x.strip()
                                    data1 = data1 + float(x)
                                    y = content_phi4[i]
                                    y = y.strip()
                                    data2 = data2 + float(y)
                                    no_data += 1
                            c_44 = data1/no_data - N_t*data2*data2/(no_data*no_data)

    for subdir, dirs, files in os.walk(c24_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile1:
                    with open(os.path.join(phi_fourth_dir,f), 'r') as myfile2:
                        with open(os.path.join(phi_sq_dir,f), 'r') as myfile3:
                            head, tail = os.path.split(c24_dir)
                            delta = f[:-4].split("_")[1]
                            if int(delta) != t:
                                pass
                            else:
                                data1 = 0
                                data2 = 0
                                data3 = 0
                                no_data = 0
                                content_44 = myfile1.readlines()
                                content_phi4 = myfile2.readlines()
                                content_phi2 = myfile3.readlines()
                                for i in range(0,len(content_22)):
                                    if i == jk_omit:
                                        pass
                                    else:
                                        x = content_44[i]
                                        x = x.strip()
                                        data1 = data1 + float(x)
                                        y = content_phi4[i]
                                        y = y.strip()
                                        data2 = data2 + float(y)
                                        z = content_phi2[i]
                                        z = z.strip()
                                        data3 = data3 + float(z)
                                        no_data += 1
                                c_24 = data1/no_data - N_t*data2*data3/(no_data*no_data)
                        
    c = np.matrix([[c_22, c_24], [c_24, c_44]])
    return c

def sqrt_c_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit=None):
    c_t0 = c_matrix_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit)
    l, U = LA.eig(c_t0)
    sqrt_diag = np.matrix([[math.sqrt(l[0]), 0], [0, math.sqrt(l[1])]])
    sq_c = U*sqrt_diag*np.transpose(U)
    return sq_c

def e_vals_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, jk_omit=None):
    c = c_matrix_even(c22_dir,c24_dir,c44_dir,phi_sq_dir,phi_fourth_dir, t, jk_omit)
    sq_c = sqrt_c_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit)
    sq_c_inv = LA.inv(sq_c)
    W = sq_c_inv*c*sq_c_inv
    l,v = LA.eig(W)
    
    #if l[0]>l[1]:
        #return -math.log(l[0])/(t-t0), -math.log(l[1])/(t-t0), math.log(l[1])/math.log(l[0])
    #else:
        #return -math.log(l[1])/(t-t0), -math.log(l[0])/(t-t0), math.log(l[0])/math.log(l[1])
    
    c = c_matrix_even(c22_dir,c24_dir,c44_dir,phi_sq_dir,phi_fourth_dir, t+1, jk_omit)
    sq_c = sqrt_c_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit)
    sq_c_inv = LA.inv(sq_c)
    W = sq_c_inv*c*sq_c_inv
    l_1,v_1 = LA.eig(W)
    
    logs = [math.log(l_1[0]),math.log(l_1[1]),math.log(l[0]),math.log(l[1])]
    logs = sorted(logs)
    
    E = [logs[3]-logs[2], logs[1]-logs[0]]
    E = sorted(E)
    
    return E[0], E[1], E[1]/E[0]
    
def e_vals_error_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, E0, E1):
    #First find how many measurements we have
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c22_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        length = len(content)
    
    var0 = []
    var1 = []
    var_ratio = []
    for j in range(0, length):
        e0, e1, ratio = e_vals_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, j)
        var0.append((E0-e0)**2)
        var1.append((E1-e1)**2)
        var_ratio.append((E1/E0 - ratio)**2)
    dE0 = math.sqrt((length-1)*sum(var0)/length)
    dE1 = math.sqrt((length-1)*sum(var1)/length)
    dR = math.sqrt((length-1)*sum(var_ratio)/length)
    return dE0, dE1, dR

def e_vals_plot_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0):
    max_delta = 0
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    t_list = []
    E0_list = []
    E1_list = []
    dE0_list = []
    dE1_list = []
    for t in range(t0+1,int(math.ceil(max_delta/2))):
        try:
            E0, E1, R = e_vals_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t)
            dE0, dE1, dR = e_vals_error_even(c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, E0, E1)
            print R, "+-", dR
            t_list.append(t)
            E0_list.append(E0)
            E1_list.append(E1)
            dE0_list.append(dE0)
            dE1_list.append(dE1)
        except:
            pass
    
    plt.errorbar(t_list, E0_list, yerr=dE0_list, fmt='k,--', label='$E_{2}$')
    plt.errorbar(t_list, E1_list, yerr=dE1_list, fmt='k,:', label='$E_{4}$')
    
    plt.legend(fontsize=12,loc=2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
#    plt.axes().set_aspect(5)
    plt.xlabel('$t$')
    plt.ylabel('$E_{k}$')
#    plt.xlim((0,10))
#    plt.ylim((0,1.2))
    pp = PdfPages('even_spectrum.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    
    return

#These 3 functions combine both odd and even parity states to give the full spectrum.
def e_vals(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, jk_omit=None):
    c_odd = c_matrix_odd(c11_dir,c13_dir,c33_dir, t, jk_omit)
    sq_c_odd = sqrt_c_odd(c11_dir,c13_dir,c33_dir, t0, jk_omit)
    sq_c_odd_inv = LA.inv(sq_c_odd)
    W = sq_c_odd_inv*c_odd*sq_c_odd_inv
    l1,v1 = LA.eig(W)
    
    c_even = c_matrix_even(c22_dir,c24_dir,c44_dir,phi_sq_dir,phi_fourth_dir, t, jk_omit)
    sq_c_even = sqrt_c_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit)
    sq_c_even_inv = LA.inv(sq_c_even)
    W = sq_c_even_inv*c_even*sq_c_even_inv
    l2, v2 = LA.eig(W)
    
    c_odd_1 = c_matrix_odd(c11_dir,c13_dir,c33_dir, t+1, jk_omit)
    sq_c_odd_1 = sqrt_c_odd(c11_dir,c13_dir,c33_dir, t0, jk_omit)
    sq_c_odd_inv_1 = LA.inv(sq_c_odd_1)
    W_1 = sq_c_odd_inv_1*c_odd_1*sq_c_odd_inv_1
    l1_1,v1_1 = LA.eig(W_1)
    
    c_even_1 = c_matrix_even(c22_dir,c24_dir,c44_dir,phi_sq_dir,phi_fourth_dir, t+1, jk_omit)
    sq_c_even_1 = sqrt_c_even(c22_dir,c24_dir,c44_dir, phi_sq_dir, phi_fourth_dir, t0, jk_omit)
    sq_c_even_inv_1 = LA.inv(sq_c_even_1)
    W_1 = sq_c_even_inv_1*c_even_1*sq_c_even_inv_1
    l2_1, v2_1 = LA.eig(W_1)
    
    logs = [math.log(l1[0]),math.log(l1[1]),math.log(l2[0]),math.log(l2[1]),math.log(l1_1[0]),math.log(l1_1[1]),math.log(l2_1[0]),math.log(l2_1[1])]
    logs = sorted(logs)
    
    E = [logs[7]-logs[6], logs[5]-logs[4], logs[3] - logs[2], logs[1] - logs[0]]
    E = sorted(E)
    
    return E[0], E[1], E[2], E[3], E[1]/E[0], E[2]/E[0], E[3]/E[0], 0.25*(E[1]**2 - 4*(E[0]**2)) 
    
    
    #evals = [l1[0],l1[1],l2[0],l2[1]]
    #evals = sorted(evals)
    #E2 = -math.log(evals[2])/(t-t0)
    #E1 = -math.log(evals[3])/(t-t0)
    #return -math.log(evals[3])/(t-t0), -math.log(evals[2])/(t-t0), -math.log(evals[1])/(t-t0), -math.log(evals[0])/(t-t0), math.log(evals[2])/math.log(evals[3]), math.log(evals[1])/math.log(evals[3]), math.log(evals[0])/math.log(evals[3]), 0.25*(E2**2 - 4*(E1**2))

def e_vals_error(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, E1, E2, E3, E4):
    #First find how many measurements we have
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    head, tail = os.path.split(c22_dir)
                    delta = f[:-4].split("_")[1]
                    if int(delta) != t:
                        pass
                    else:
                        data = 0
                        no_data = 0
                        content = myfile.readlines()
                        length = len(content)
    
    var1 = []
    var2 = []
    var3 = []
    var4 = []
    var_R1 = []
    var_R2 = []
    var_R3 = []
    var_P2 = []
    R1 = E2/E1
    R2 = E3/E1
    R3 = E4/E1
    P2 = 0.25*(E2**2 - 4*(E1**2))
    for j in range(0, length):
        e1, e2, e3, e4, r1, r2, r3, p2 = e_vals(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, j)
        var1.append((E1-e1)**2)
        var2.append((E2-e2)**2)
        var3.append((E3-e3)**2)
        var4.append((E4-e4)**2)
        var_R1.append((R1 - r1)**2)
        var_R2.append((R2 - r2)**2)
        var_R3.append((R3 - r3)**2)
        var_P2.append((P2 - p2)**2)
    dE1 = math.sqrt((length-1)*sum(var1)/length)
    dE2 = math.sqrt((length-1)*sum(var2)/length)
    dE3 = math.sqrt((length-1)*sum(var3)/length)
    dE4 = math.sqrt((length-1)*sum(var4)/length)
    dR1 = math.sqrt((length-1)*sum(var_R1)/length)
    dR2 = math.sqrt((length-1)*sum(var_R2)/length)
    dR3 = math.sqrt((length-1)*sum(var_R3)/length)
    dP2 = math.sqrt((length-1)*sum(var_P2)/length)
    return dE1, dE2, dE3, dE4, dR1, dR2, dR3, dP2
    
def e_vals_plot(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0):
    max_delta = 0
    for subdir, dirs, files in os.walk(c22_dir):
        for f in files:
            if f.split('.')[1] != 'txt':
                pass
            else:
                with open(os.path.join(subdir,f),'r') as myfile:
                    delta = int(f[:-4].split("_")[1])
                    if delta>max_delta:
                        max_delta = delta
    t_list = []
    E1_list = []
    E2_list = []
    E3_list = []
    E4_list = []
    dE1_list = []
    dE2_list = []
    dE3_list = []
    dE4_list = []
    R0_list = []
    R1_list = []
    R2_list = []
    R3_list = []
    P2_list = []
    dR0_list = []
    dR1_list = []
    dR2_list = []
    dR3_list = []
    dP2_list = []
    for t in range(t0+1,int(math.ceil(max_delta/2))):
        try:
            E1, E2, E3, E4, R1, R2, R3, P2 = e_vals(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t)
            dE1, dE2, dE3, dE4, dR1, dR2, dR3, dP2 = e_vals_error(c11_dir, c13_dir, c33_dir, c22_dir, c24_dir, c44_dir, phi_sq_dir, phi_fourth_dir, t0, t, E1, E2, E3, E4)
            t_list.append(t)
            E1_list.append(E1)
            E2_list.append(E2)
            E3_list.append(E3)
            E4_list.append(E4)
            R0_list.append(1)
            R1_list.append(R1)
            R2_list.append(R2)
            R3_list.append(R3)
            P2_list.append(P2)
            dE1_list.append(dE1)
            dE2_list.append(dE2)
            dE3_list.append(dE3)
            dE4_list.append(dE4)
            dR0_list.append(0)
            dR1_list.append(dR1)
            dR2_list.append(dR2)
            dR3_list.append(dR3)
            dP2_list.append(dP2)
        except:
            pass

    plt.errorbar(t_list, E1_list, yerr=dE1_list, fmt='k,:', color='#067c0c', label='$E_{1}$')
    plt.errorbar(t_list, E2_list, yerr=dE2_list, fmt='k,--', color='#3e1191', label='$E_{2}$')
    plt.errorbar(t_list, E3_list, yerr=dE3_list, fmt='k,-.', color='#cc0014', label='$E_{3}$')
    plt.errorbar(t_list, E4_list, yerr=dE4_list, fmt='k,-', color='#a32c79', label='$E_{4}$')
    
    plt.legend(fontsize=12,loc=2)
    plt.xlim(0, len(t_list)+2)
    #rc('xtick', labelsize=50) 
    #rc('ytick', labelsize=50)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
    #plt.axes().set_aspect((5.3))
    #plt.xlabel('$t$')
    #plt.ylabel('$E^{\\textrm{eff}}_{n}(t,t_{0}=4)$')
    #plt.xlim((0,len(t_list)+2))
    #plt.ylim((0,1.2))
    pp = PdfPages('spectrum.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    
    #print "t =", t_list[0]
    #print E1_list[0], dE1_list[0]
    #print E2_list[0], dE2_list[0]
    #print E3_list[0], dE3_list[0]
    #print E4_list[0], dE4_list[0]
    #print P2_list[0], dP2_list[0]
    #print R2_list, dR2_list
    plt.errorbar(t_list, R0_list, yerr=dR0_list, fmt='k,:', color='#067c0c', label='$E_{1}/E_{1}$')
    plt.errorbar(t_list, R1_list, yerr=dR1_list, fmt='k,--',color='#3e1191', label='$E_{2}/E_{1}$')
    plt.errorbar(t_list, R2_list, yerr=dR2_list, fmt='k,-.', color='#cc0014', label='$E_{3}/E_{1}$')
    plt.errorbar(t_list, R3_list, yerr=dR3_list, fmt='k,-',  color='#a32c79', label='$E_{4}/E_{1}$')
    
    plt.legend(fontsize=12,loc=2)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
    #plt.axes().set_aspect(1.0)
    plt.xlabel('$t$')
    plt.ylabel('$E_{k}/E_{1}$')
    plt.xlim(0,len(t_list)+1)
    plt.ylim((0,6))
    pp = PdfPages('ratio_spectrum.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    
    plt.errorbar(t_list, P2_list, yerr=dP2_list, fmt='k,:')
    plt.legend(fontsize=12,loc=2)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.rcParams.update({'font.size': 20})
    #plt.axes().set_aspect(1.0)
    plt.xlabel('$t$')
    plt.ylabel('$p^{*2}$')
    plt.xlim((0,len(t_list)+2))
    #plt.ylim((0,6))
    pp = PdfPages('threshold.pdf')
    pp.savefig(bbox_inches='tight')
    pp.close()
    plt.clf()
    return



plot_MC_steps('/home/david/FinalYearProject/data/tests/08Mar_9/delta_28.txt')
#plot_correlation('/home/david/FinalYearProject/data/10_correlations/29Jan_2_crop_bin')
#e_vals_plot_even('/home/david/FinalYearProject/data/22_correlations/06Feb_1_crop_bin','/home/david/FinalYearProject/data/24_correlations/06Feb_1_crop_bin','/home/david/FinalYearProject/data/44_correlations/06Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_sq/06Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_fourth/06Feb_1_crop_bin', 0)
#extract_tauint('/home/david/FinalYearProject/data/a-c_tests_5/raw_data',2000,500)
#e_vals_plot_odd('/home/david/FinalYearProject/data/11_correlations/09Feb_1_crop_bin','/home/david/FinalYearProject/data/13_correlations/09Feb_1_crop_bin','/home/david/FinalYearProject/data/33_correlations/09Feb_1_crop_bin',0)
#e_vals_plot_odd('/home/david/FinalYearProject/data/11_correlations/15Jan_1_crop_bin','/home/david/FinalYearProject/data/13_correlations/15Jan_1_crop_bin','/home/david/FinalYearProject/data/33_correlations/15Jan_1_crop_bin',0)
#e_vals_plot('/home/david/FinalYearProject/data/22_correlations/31Jan_1_crop_bin','/home/david/FinalYearProject/data/24_correlations/31Jan_1_crop_bin','/home/david/FinalYearProject/data/44_correlations/31Jan_1_crop_bin',3)
#sort_raw_data('/home/david/FinalYearProject/data/a-c_tests_5/raw_data',2000)
#plot_tauint('/home/david/FinalYearProject/data/auto-correlation_tests/15Dec_23')
#rebin('/home/david/FinalYearProject/data/phi_fourth/06Feb_1_crop',100)
#jackknife_log_ratio('/home/david/FinalYearProject/seagull_data/data/33_correlations/02Mar_2_crop_bin_edited')
#jackknife_log('/home/david/FinalYearProject/data/11_correlations/15Feb_1_crop_bin')
#dispersion_plot()
#jackknife()


#subtract_vacuum('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_2_crop_bin',47042.2896034)
#subtract_vacuum('/home/david/FinalYearProject/seagull_data/data/13_correlations/02Mar_2_crop_bin',1079563344.33)
#subtract_vacuum('/home/david/FinalYearProject/seagull_data/data/33_correlations/02Mar_2_crop_bin',2.43899228203e+13)

#crop('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_2',10000)
#rebin('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_2_crop',10)

##########################
#15th FEBRUARY DATA
##########################
#e_vals_plot_odd('/home/david/FinalYearProject/data/11_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/13_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/33_correlations/15Feb_1_crop_bin',0)
#e_vals_plot_even('/home/david/FinalYearProject/data/22_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/24_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/44_correlations/15Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_sq/15Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_fourth/15Feb_1_crop_bin', 0)
#e_vals_plot('/home/david/FinalYearProject/data/11_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/13_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/33_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/22_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/24_correlations/15Feb_1_crop_bin','/home/david/FinalYearProject/data/44_correlations/15Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_sq/15Feb_1_crop_bin', '/home/david/FinalYearProject/data/phi_fourth/15Feb_1_crop_bin', 0)

##########################
#SEAGULL DATA
##########################
#e_vals_plot_odd('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/13_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/33_correlations/02Mar_1_crop_bin',0)
#e_vals_plot_even('/home/david/FinalYearProject/seagull_data/data/22_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/24_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/44_correlations/02Mar_1_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_sq/02Mar_1_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_fourth/02Mar_1_crop_bin', 0)
#e_vals_plot('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/13_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/33_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/22_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/24_correlations/02Mar_1_crop_bin','/home/david/FinalYearProject/seagull_data/data/44_correlations/02Mar_1_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_sq/02Mar_1_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_fourth/02Mar_1_crop_bin', 0)

#e_vals_plot_odd('/home/david/FinalYearProject/seagull_data/data/11_correlations/02Mar_2_crop_bin_edited','/home/david/FinalYearProject/seagull_data/data/13_correlations/02Mar_2_crop_bin_edited','/home/david/FinalYearProject/seagull_data/data/33_correlations/02Mar_2_crop_bin_edited',0)
#e_vals_plot_even('/home/david/FinalYearProject/seagull_data/data/22_correlations/02Mar_2_crop_bin','/home/david/FinalYearProject/seagull_data/data/24_correlations/02Mar_2_crop_bin','/home/david/FinalYearProject/seagull_data/data/44_correlations/02Mar_2_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_sq/02Mar_2_crop_bin', '/home/david/FinalYearProject/seagull_data/data/phi_fourth/02Mar_2_crop_bin', 0)
