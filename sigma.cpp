#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib> 
#include <typeinfo>
#include <fstream>
#include <string.h>
#include <math.h>

using namespace std;

////////////////////////       
////////////////////////

void print_coords(int* coords) //Prints out the array [t,x,y,z]
{
    cout<<"["<<coords[0]<<","<<coords[1]<<","<<coords[2]<<","<<coords[3]<<"]";
    return;
}

int mod(int a, int b) //Returns a%b, with correct behaviour for a<0.
{
   int ret = a % b;
   if(ret < 0)
     ret+=b;
   return ret;
}

double rand_normal(double mean, double stddev)
{//Box muller method
    static double n2 = 0.0;
    static int n2_cached = 0;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);
        {
            double d = sqrt(-2.0*log(r)/r);
            double n1 = x*d;
            n2 = y*d;
            double result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2*stddev + mean;
    }
}

void test_bijection(lattice L)
{
    for(int i=0; i<L.number_points(); i++)
    {
        int* coords = L.int_to_coords(i);
        int grid_number = L.coords_to_int(coords[0],coords[1],coords[2],coords[3]);
        int diff = i-grid_number;
        if(diff!=0)
            cout<<diff;
    }
}

////////////////////////
////////////////////////

template <int d>
class sigma
{
    private:
        int dim;
        double am;
        double l;
        
    public:
        float s[d];
        sigma();
        
        int read_dim();
        double read_am();
        double read_l();
        
        float operator*(sigma<d> sig)
        {
            float dot_product = 0;
            for(int i=0; i<d; i++)
            {
                dot_product += this->s[i]*sig.s[i];
            }
            return dot_product;
        }
};

template <int d>
sigma<d>::sigma()
{
    dim = d;
    am = 0.01;
    l = 1.0;
}

template <int d>
int sigma<d>::read_dim()
{
    return dim;
}

template <int d>
double sigma<d>::read_am()
{
    return am;
}

template <int d>
double sigma<d>::read_l()
{
    return l;
}

////////////////////////
////////////////////////

template <class field_class>
class lattice
{
    private:
        int N_t;
        int N_x;
    public:
        field_class* field; //The field itself, implemented as a one dimensional array with elements of type field_class
        int number_points(); //Return N_t*N_x*N_x*N_x
        void set_grid(int n_t, int n_x); //Set the private variables N_t, N_x
        void create_field(); //Allocates memory for the field, and initialises it to zero
        
        int** NN; //Nearest neighbours, NN[i] gives an array with the 8 NN of lattice site i
        void save_nearest_neighbours(); //Calculates nearest neighbours and saves to NN
        
        int coords_to_int(int t, int x, int y, int z); //These two give the bijection between 4-coords [t,x,y,z] and the single coord system
        int* int_to_coords(int grid_number);
        
        field_class vary_point(int i, double r); //Returns a changed point without changing the field class object
        void vary_field(int i, double r); //Permanently changes the field at site i
        double deltaS(int i, double r); //Return deltaS when site i is varied by r
        int metropolis_step(int i, double std); //Performs one metropolis alg. step, at site i. Returns 1 if the site is permanently varied, returns 0 otherwise.
        
        double* time_slice(int n_x, int n_y, int n_z); //Forms the interpolation field, with momentum p = (2pi/N_x)*(n_x,n_y,n_z)
        double correlation_11(int delta, double* time_sliced_array); //All the various correlation functions
        double correlation_13(int delta, double* time_sliced_array);
        double correlation_33(int delta, double* time_sliced_array);
        double correlation_22(int delta, double* time_sliced_array);
        double correlation_24(int delta, double* time_sliced_array);
        double correlation_44(int delta, double* time_sliced_array);
        double phisq_expect(int delta, double* time_sliced_array);
        double phi4_expect(int delta, double* time_sliced_array);
        double correlation_10(int delta, double* time_sliced_array);
        double correlation_21(int delta, double* time_sliced_array);
        
        //Some functions that are good for benchmarking, but not much else
        void print_field();
        void print_field_long();
        void project_zy(int x_val, int t_val);
        void translate_field(int t_translate, int x_translate, int y_translate, int z_translate);
        double laplacian(int i, int comp); //Evaluates the Laplacian at a point (int i) on the grid, for the component "comp" = 0,1,2,3 of the field.
        field_class vector_laplacian(int i); //Evaluates the Laplacian of each component of a vector, and returns a vector with these components
        double simple_action();
        double action();

};

template <class field_class>
int lattice<field_class>::number_points() //Returns the number of points in the lattice
{
    return N_x*N_x*N_x*N_t;
}

template <class field_class>
void lattice<field_class>::set_grid(int n_t, int n_x) //Sets the lattice parameters
{
    N_t = n_t;
    N_x = n_x;
}

template <class field_class>
void lattice<field_class>::create_field() //Initialises the field and allocates memory for it.
{
    int N = number_points();
    
    field_class* field_init = (field_class*)malloc(N*sizeof(field_class));
    for(int i=0; i < N; i++)
    {
        field_class field_point;
        int field_dim = field_point.read_dim(); 
        for(int j = 0; j<field_dim; j++)
        {
            field_point.s[j] = 0;
        }
        field_init[i] = field_point;
    }
    field = field_init;
}


template <class field_class>
void lattice<field_class>::save_nearest_neighbours() 
//Calculate and save the nearest neighbours, NN. NN[i] gives an array of length 8: the 8 nearest neighbours of point i.
{
    int N = number_points();
    int** nn = (int**)malloc(N*sizeof(int*));
    for(int i=0; i<N; i++)
    {
        nn[i] = (int*)malloc(8*sizeof(int));
        int* coords = int_to_coords(i);
        int t = coords[0];
        int x = coords[1];
        int y = coords[2];
        int z = coords[3];
        
        nn[i][7] = coords_to_int(t,x,y,z-1);
        nn[i][6] = coords_to_int(t,x,y,z+1);
        nn[i][5] = coords_to_int(t,x,y-1,z);
        nn[i][4] = coords_to_int(t,x,y+1,z);
        nn[i][3] = coords_to_int(t,x-1,y,z);
        nn[i][2] = coords_to_int(t,x+1,y,z);
        nn[i][1] = coords_to_int(t-1,x,y,z);
        nn[i][0] = coords_to_int(t+1,x,y,z);
    }
    NN=nn;
    return;
}

template <class field_class>
int lattice<field_class>::coords_to_int(int t, int x, int y, int z)
{
    int t_2 = mod(t,N_t);
    int x_2 = mod(x,N_x);
    int y_2 = mod(y,N_x);
    int z_2 = mod(z,N_x);
    return z_2 + y_2*N_x + x_2*N_x*N_x + t_2*N_x*N_x*N_x;
}

template <class field_class>
int* lattice<field_class>::int_to_coords(int grid_number)
{
    int gn_2 = mod(grid_number,number_points());
    
    int* coords = (int*)malloc(4*sizeof(int));
    coords[3] = mod(gn_2,N_x);
    gn_2 = gn_2/N_x;
    coords[2] = mod(gn_2,N_x);
    gn_2 = gn_2/N_x;
    coords[1] = mod(gn_2,N_x);
    gn_2 = gn_2/N_x;
    coords[0] = mod(gn_2,N_t);
    return coords;
}


template <class field_class> //Vary site i by r, but don't set the field class variable to this varied value 
field_class lattice<field_class>::vary_point(int i, double r)
{
    field_class var_point;
    var_point.s[0] = field[i].s[0] + r;
    return var_point;
}

template <class field_class>//Vary site i by r, and do set the field class variable to this varied value 
void lattice<field_class>::vary_field(int i, double r)
{
    field_class var_point = vary_point(i,r);
    field[i] = var_point;
    return;
}

template <class field_class>
double lattice<field_class>::deltaS(int i, double r) //Change in the action when changing site i, with (random) change r.
{
    field_class point = field[i];
    field_class var_point = vary_point(i,r);
    double var_sq = var_point*var_point;
    double point_sq = point*point;
    double am = point.read_am();
    double l = point.read_l();
    int* nn = NN[i];
    double NN_sum = 0;
    for(int j = 0; j<8; j++)
    {
        NN_sum += field[nn[j]].s[0];
    }
    double deltaS = 0.5*(8+am*am)*(var_sq - point_sq) + l*(var_sq*var_sq - point_sq*point_sq)/24 - (var_point.s[0] - point.s[0])*(NN_sum);
    return deltaS;
}

template <class field_class>
int lattice<field_class>::metropolis_step(int i, double std)
{
    field_class field_point;
    //double S = action();
    double am = field_point.read_am(); 
    double r = rand_normal(0,std);
    //double r = (((double) rand() / (RAND_MAX))*2 - 1);
    double dS = deltaS(i,r);
    int varied = 0;
    if(exp(-dS) >= 1)
    {
        vary_field(i,r);
        varied = 1;
    }
    else
    {
        double random = ((double) rand() / (RAND_MAX));
        if( exp(-dS)>random )
        {
            vary_field(i,r);
            varied = 1;
        }
    }
    
    //For checking that deltaS(i,r) is consistent with action() 
//     if(varied==1)
//     {
//         double S_1 = action();
//         double dS_full = S_1 - S;
//         cout<<"d(dS) = "<<dS-dS_full<<"\n";
//     }
     return varied;
}

template <class field_class>
double* lattice<field_class>::time_slice(int n_x, int n_y, int n_z)
{
    int N = number_points();
    double* time_sliced_array = (double*)malloc(N_t*sizeof(double));
    for(int t=0; t<N_t; t++)
    {
        time_sliced_array[t] = 0;
        for(int x=0; x<N_x; x++)
            for(int y=0; y<N_x; y++)
                for(int z=0; z<N_x; z++)
                {
                    int i = coords_to_int(t,x,y,z);
                    time_sliced_array[t] += cos( (2*M_PI/N_x)*(n_x*x + n_y*y + n_z*z) )*field[i].s[0];
                }
        
    }
    return time_sliced_array;
}

template <class field_class>
double lattice<field_class>::correlation_11(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_13(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_33(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_22(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_24(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_44(int delta, double* time_sliced_array)
{
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}

template <class field_class>
double lattice<field_class>::phisq_expect(int delta, double* time_sliced_array)
{
    double phi_sq = time_sliced_array[delta]*time_sliced_array[delta];
    return phi_sq;
}

template <class field_class>
double lattice<field_class>::phi4_expect(int delta, double* time_sliced_array)
{
    double phi_fourth = time_sliced_array[delta]*time_sliced_array[delta]*time_sliced_array[delta]*time_sliced_array[delta];
    return phi_fourth;
}

template <class field_class>
double lattice<field_class>::correlation_10(int delta, double* time_sliced_array)
{
    //Just a sanity check that this does give 0.
    double c = time_sliced_array[delta];
    return c;
}

template <class field_class>
double lattice<field_class>::correlation_21(int delta, double* time_sliced_array)
{
    //Just a sanity check that this does give 0.
    double c = 0;
    for(int t=0; t<N_t; t++)
    {
        c += time_sliced_array[t]*time_sliced_array[mod(t+delta,N_t)]*time_sliced_array[mod(t+delta,N_t)];
    }
    return c;
}


template <class field_class>
void lattice<field_class>::print_field()
{
    int N = number_points();
    field_class point;
    int dim = point.read_dim();
    for(int i=0; i<N; i++)
    {
        cout<<"[";
        for(int j=0; j<dim-1; j++)
        {
            cout<<field[i].s[j]<<",";
        }
        cout<<field[i].s[dim-1]<<"] ";
    }
}

template <class field_class>
void lattice<field_class>::print_field_long()
{
    int N = number_points();
    field_class point;
    int dim = point.read_dim();
    ofstream myfile;
    myfile.open("field.txt");
    for(int i=0; i<N; i++)
    {
        for(int j=0; j<dim; j++)
        {
            myfile<<field[i].s[j]<<"\n";
        }
    }
}

template <class field_class>
void lattice<field_class>::project_zy(int t_val, int x_val)
{
    int N = number_points();
    int index = 0;
    double temp_array [N_x*N_x];
    
    for(int i=0; i<N; i++)
    {
        int* coords = int_to_coords(i);
        int t = coords[0];
        int x = coords[1];
        if(x==x_val && t==t_val && index == 0)
        {
            temp_array[0] = field[i].s[0];
            index += 1;
        }
        else if(index!=0 && index<N_x*N_x)
        {
            temp_array[index] = field[i].s[0];
            index += 1;
        }
    }
    
    int x = 0;
    int y = 0;
    for(int i=0; i<N_x*N_x; i++)
    {
//         if(mod(i,N_x)==0)
//         {
//             cout<<"\n";
//         }
        
        cout<<temp_array[i]<<"\n";
    }
}

template <class field_class>
void lattice<field_class>::translate_field(int t_translate, int x_translate, int y_translate, int z_translate)
{
    int N = number_points();
    field_class* new_field = (field_class*)malloc(N*sizeof(field_class));
    for(int i = 0; i<N; i++)
    {
        field_class point = field[i];
        int* coords = int_to_coords(i);
        int new_t = coords[0] + t_translate;
        int new_x = coords[1] + x_translate;
        int new_y = coords[2] + y_translate;
        int new_z = coords[3] + z_translate;
        int new_grid_number = coords_to_int(new_t,new_x,new_y,new_z);
        new_field[new_grid_number] = point;
    }
    field = new_field;
    return;
}

template <class field_class>
double lattice<field_class>::simple_action()
{
     int N = number_points();
     double action = 0;
     for(int i = 0; i<N; i++)
     {
         field_class point = field[i];
         int* nn = NN[i];
         for(int j = 0; j<8; j++)
         {
             action += (1-point*field[NN[j]]);
         }
     }
     return action;
}

template <class field_class>
double lattice<field_class>::action()
{
     int N = number_points();
     double action = 0;
     for(int i = 0; i<N; i++)
     {
         field_class point = field[i];
         double am = point.read_am();
         double l = point.read_l();
         int* nn = NN[i];
         double NN_interaction = 0;
         for(int j=0; j<8; j++)
         {
             NN_interaction += 0.5*(point*field[nn[j]]);
         }
         action += 0.5*(8+am*am)*(point*point) - NN_interaction + l*(point*point)*(point*point)/24;
     }
     return action;
}


////////////////////////
////////////////////////


int main()
{
    srand (time(NULL));
    
    lattice<sigma<1> > L;
    int N_t = 50;
    int N_x = 16;
    L.set_grid(N_t,N_x);
    unsigned long N = L.number_points();
    L.create_field();
    L.save_nearest_neighbours();
    
    int metro_steps = 50;
    int no_measure = 2000;
    double gauss_std = 0.7;
    int p_x = 0;
    int p_y = 0;
    int p_z = 0;
    int burn_in = 0;
    
    unsigned long no_varied = 0;
    
    cout<<"N_x = "<<N_x<<"\n";
    cout<<"N_t = "<<N_t<<"\n";
    cout<<"metro_steps = "<<metro_steps<<"\n";
    cout<<"am = "<<L.field[0].read_am()<<"\n";
    cout<<"lambda = "<<L.field[0].read_l()<<"\n";
    cout<<"gauss_std = "<<gauss_std<<"\n";
    cout<<"Burn-in = "<<burn_in<<"\n";
    cout<<"p = ("<<p_x<<","<<p_y<<","<<p_z<<")\n";

    for(int k = 0; k<no_measure + burn_in; k++)
    {
        for(int i = 0; i<metro_steps; i++)
            {
                for(int j=0; j<N; j++)
                {
                    int varied = L.metropolis_step(j, gauss_std);
                    no_varied += varied;
                }
            }
        if(k>burn_in)
        {
            for(int delta=0; delta<N_t; delta++)
            {
                double* time_sliced_array = L.time_slice(p_x, p_y, p_z);
                double c_11 = L.correlation_11(delta, time_sliced_array);
                
                ofstream myfile1;
                char delta_string[32];
                sprintf(delta_string, "%d", delta);
                char other_string1[64] = "./data/tests/08Mar_11/delta_";
                strcat(other_string1, delta_string);
                strcat(other_string1, ".txt");
                myfile1.open(other_string1, ios::out | ios::app);
                myfile1<<c_11<<"\n";

                //REMEMBER TO FREE INTERPOLATION FIELD (AND ANYTHING ELSE) TO AVOID LEAKS
                free(time_sliced_array);
            }
        }
    }
    cout<<"acceptance = "<<no_varied<<"/"<<((no_measure+burn_in)*N*metro_steps)<<"\n";
    
    return 0;
}

////////////////////////       
////////////////////////
